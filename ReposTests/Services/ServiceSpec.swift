//
//  ServiceSpec.swift
//  Repos
//
//  Created by funkill on 02/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import Repos

class ServiceSpec: QuickSpec {

  private var service: StubService!

  override func spec() {
    beforeEach {
      self.service = StubService()
    }

    describe("Service protocol extension") {
      it("can prepare response object") {
        let response = Response(code: 0, headers: nil, body: nil)
        let result = ResponseResult.success(response)

        let preparedResult = self.service.prepareResponse(result)

        XCTAssertNotNil(preparedResult)
      }

      describe("check response result") {
        it("return error, if it prepared error response") {
          let error = NSError(domain: "some error", code: 0, userInfo: nil) as Error
          let result = ResponseResult.error(error)

          let preparedResult = self.service.prepareResponse(result)

          XCTAssertTrue(preparedResult.isError())
        }

        it("return error, if it prepared result with status code not equals 200") {
          let response = Response(code: 300, headers: nil, body: nil)
          let result = ResponseResult.success(response)

          let preparedResult = self.service.prepareResponse(result)

          XCTAssertTrue(preparedResult.isError())
        }

        it("return dto, if it prepared result woith status code 200") {
          let response = Response(code: 200, headers: nil, body: nil)
          let result = ResponseResult.success(response)

          let preparedResult = self.service.prepareResponse(result)

          XCTAssertTrue(preparedResult.isSuccess())
        }
      }
    }
  }
}

private class StubService: NetworkService {
  static var dtoClass: DataTransferObject.Type {
    return DTO.self
  }
}

// swiftlint:disable:next type_name
private struct DTO: DataTransferObject {
  static func mapped(_ json: String) -> DTO {
    return DTO()
  }
}
