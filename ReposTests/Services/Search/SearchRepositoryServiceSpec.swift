//
// Created by funkill on 01/01/17.
// Copyright (c) 2017 E-legion. All rights reserved.
//

import Foundation
import Nimble
import Quick

@testable import Repos

class SearchRepositoryServiceSpec: QuickSpec {

  private var service: SearchRepositoriesService!

  override func spec() {
    describe("SearchRepositoryService") {
      it("can be creatable") {
        _ = SearchRepositoriesService()
      }

      describe("Search repository service behavior") {
        beforeEach {
          self.service = SearchRepositoriesService()
        }

        it("can search repositories by query") {
          self.service.searchBy("some query", callback: { _ in })
        }

        it("can get concrete page from result") {
          self.service.searchBy("some query", page: 2, callback: { _ in })
        }

        it("call callback on get result") {
          let expect = self.expectation(description: "Check calling callback on get result")
          self.service.searchBy("some query", callback: {
            result in
            switch result {
              case .success(_): break
              case let .error(error):
                XCTFail("Test fails with error: \(String(describing: error))")
            }

            expect.fulfill()
          })

          self.waitForExpectations(timeout: 60)
        }
      }
    }
  }
}
