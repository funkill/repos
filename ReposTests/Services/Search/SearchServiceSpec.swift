//
//  SearchServiceSpec.swift
//  Repos
//
//  Created by funkill on 03/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import Repos

class SearchServiceSpec: QuickSpec {

  private var service: StubSearchService!
  // inline because when i use variable, xcode can't understand it
  private var defaultResult = ResponseResult.success(Response(code: 200, headers: [:], body: Data()))

  override func spec() {
    beforeEach {
      self.service = StubSearchService()
    }

    describe("SearchService protocol") {
      it("run callback after") {
        self.service.result = self.defaultResult
        self.service.searchBy(callback: { _ in })
      }

      it("run callback with error") {
        let error = NSError(domain: "some error", code: -1, userInfo: nil) as Error
        self.service.result = ResponseResult.error(error)
        self.service.searchBy(callback: { error in XCTAssertTrue(error.isError()) })
      }

      it("run callback with result") {
        self.service.result = self.defaultResult
        self.service.searchBy(callback: { result in XCTAssertTrue(result.isSuccess()) })
      }
    }
  }
}

// swiftlint:disable:next type_name
private struct DTO: DataTransferObject {
  static func mapped(_ json: String) -> DTO {
    return DTO()
  }
}

private class StubSearchService: SearchService {

  var result: ResponseResult?

  static var dtoClass: DataTransferObject.Type {
    return DTO.self
  }

  func searchBy(_ query: String, page: Int, callback: @escaping (SearchResult) -> Void) {
    let result = self.prepareSearchResponse(self.result!)
    callback(result)
  }

  func searchBy(callback: @escaping (SearchResult) -> Void) {
    self.searchBy("", page: 1, callback: callback)
  }
}
