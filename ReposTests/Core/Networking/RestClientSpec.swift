//
//  RestClientTestCase.swift
//  Repos
//
//  Created by funkill on 27/11/16.
//
//

import Foundation

import Quick
import Nimble

@testable import Repos

extension RestClientConfiguration {
  static var testConfiguration: RestClientConfiguration = {
    let baseUrl = "file://" + Bundle.main.bundlePath
    let client = DummyClient.self

    return RestClientConfiguration(baseUrl: baseUrl, client: client)
  }()
}

class RestClientSpec: QuickSpec {

  fileprivate let baseUrl = Bundle.main.bundlePath
  fileprivate var stubClient: RestfulClient!

  override func spec() {

    describe("a rest client functionality") {
      var client: RestClient!

      beforeEach {
        let configuration = RestClientConfiguration.testConfiguration
        let executor = configuration.client.init(baseUrl: configuration.baseUrl)
        client = RestClient(executor)
      }

      describe("method .get()") {
        let path = "/ReposTests/Core/tests.json"

        it("can be called without body") {
          client.get(path, callback: { _ in })
        }

        it("can be called with body") {
          let body = [ "a": 1 ]
          let query = [ "a": 2 ]
          let headers = [ "Content-type": "application/json" ]
          let request = Request(body: body, query: query, headers: headers)
          client.get(path, withRequest: request, callback: { _ in })
        }

        it("returns data from response") {
          client.get(path, callback: { response in
            XCTAssertTrue(response.isSuccess())
          })
        }
      }
    }
  }
}

private class DummyClient: RestfulClient {
  private let baseUrl: String

  required init(baseUrl: String) {
    self.baseUrl = baseUrl
  }

  func get(_ path: String, withRequest request: Request, callback: @escaping ResponseCallback) {
    let headers = self.getHeaders()
    let response = Response(code: 200, headers: headers, body: nil)

    callback(ResponseResult.success(response))
  }

  private func getHeaders() -> [AnyHashable: Any] {
    return [
      "server": "nginx/1.8.0",
      "date": "Fri, 09 Dec 2016 11:33:18 GMT",
      "content-type": "application/json;charset=UTF-8",
    ]
  }
}
