//
//  FileRestClientSpec.swift
//  Repos
//
//  Created by funkill on 01/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation

import Quick
import Nimble

@testable import Repos

class FileRestClientSpec: QuickSpec {

  private let baseUrl = Bundle.main.bundlePath
  private var client: FileRestClient!

  override func spec() {
    describe("FileRestClient") {
      it("creatable") {
        _ = FileRestClient(baseUrl: self.baseUrl)
      }

      beforeEach {
        self.client = FileRestClient(baseUrl: self.baseUrl)
      }

      describe("behavior of FileRestClient") {
        it("has method get") {
          let expect = self.expectation(description: "Check result for FileRestClient")
          self.client.get("/search/repositories", withRequest: Request()) {
            result in
            switch result {
              case let .success(value):
                XCTAssertEqual(200, value.code)
                XCTAssertTrue(value.headers! is [AnyHashable: Any])
                XCTAssertTrue(value.body! is Data)

              case let .error(error):
                XCTFail("Failed with error: \(String(describing: error))")
            }
            expect.fulfill()
          }
          self.waitForExpectations(timeout: 60)
        }

        it("can return error if file not exists") {
          let expect = self.expectation(description: "Check result for FileRestClient")
          self.client.get("/not_found", withRequest: Request()) {
            result in
            switch result {
            case let .success(value):
              // swiftlint:disable:next line_length
              XCTFail("Failed with success value: \(String(describing: value)). In this test result must be an error!")

            case let .error(error):
              let _error = error as NSError
              XCTAssertEqual(-1100, _error.code)
            }
            expect.fulfill()
          }
          self.waitForExpectations(timeout: 60)
        }
      }
    }
  }

}
