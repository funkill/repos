//
//  URLRestClientSpec.swift
//  Repos
//
//  Created by funkill on 27/11/16.
//
//

import Foundation

import Quick
import Nimble

@testable import Repos

class URLRestClientSpec: QuickSpec {

  fileprivate let baseUrl = "http://example.com"
  fileprivate var client: RestfulClient!

  override func spec() {
    describe("URLRestClient") {
      it("created with base url") {
        _ = URLRestClient(baseUrl: self.baseUrl)
      }

      describe("beahavior of URLRestClient") {
        let existsPath = "/test"

        beforeEach {
          self.client = URLRestClient(baseUrl: self.baseUrl)
        }

        it("implements protocol RestClientProtocol") {
          XCTAssertTrue(self.client is RestfulClient)
        }

        it("has get method") {
          self.client.get(existsPath, withRequest: Request(), callback: { _ in })
        }

        it("can get query (as dictionary) in body") {
          let request = Request(body: [ "a": 1 ], query: [ "b": 1 ], headers: [:])
          self.client.get(existsPath, withRequest: request, callback: { _ in })
        }

        it("call callback after request") {
          let expect = self.expectation(description: "Check callback for URLRestClientSpec")
          self.client.get(existsPath, withRequest: Request(), callback: {
            _ in
            expect.fulfill()
          })

          self.waitForExpectations(timeout: 10)
        }

        it("callback get reponse with code and other data") {
          let expect = self.expectation(description: "Check response for URLRestClientSpec")
          self.client.get(existsPath, withRequest: Request(), callback: {
            response in
            switch response {
              case let .success(data):
                XCTAssertTrue(data is Response)
              case _:
              XCTFail("Response is not Ok!")
            }
            expect.fulfill()
          })

          self.waitForExpectations(timeout: 10)
        }
      }
    }
  }
}
