//
//  ResultSpec.swift
//  Repos
//
//  Created by funkill on 02/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import Repos

class ResultSpec: QuickSpec {
  typealias IntResult = Result<Int, String>
  typealias StringResult = Result<String, String>

  private var successResult: IntResult!
  private var errorResult: IntResult!

  // swiftlint:disable:next function_body_length
  override func spec() {
    beforeEach {
      self.successResult = IntResult.success(-3)

      let internalError = "Some error message"
      self.errorResult = IntResult.error(internalError)
    }

    describe("Result is a type that represents either success (success) or failure (error).") {
      it("has method isSuccess. Returns true if the result is success.") {
        XCTAssertTrue(self.successResult.isSuccess())
        XCTAssertFalse(self.errorResult.isSuccess())
      }

      it("has method isError. Returns true if the result is Error.") {
        XCTAssertFalse(self.successResult.isError())
        XCTAssertTrue(self.errorResult.isError())
      }

      // swiftlint:disable:next line_length
      it("has method getSuccess. Converts from Result<T, E> to Optional<T>. Converts self into an Optional<T>, consuming self, and discarding the error, if any.") {
        XCTAssertTrue(self.successResult.getSuccess() is Int?)
        XCTAssertTrue(self.successResult.getSuccess()! is Int)

        XCTAssertNil(self.errorResult.getSuccess())
      }

      // swiftlint:disable:next line_length
      it("has method getError. Converts from Result<T, E> to Optional<E>. Converts self into an Option<E>, consuming self, and discarding the success value, if any.") {
        XCTAssertNil(self.successResult.getError())
        XCTAssertNotNil(self.errorResult.getError())
      }

      // swiftlint:disable:next line_length
      it("has method map. Maps a Result<T, E> to Result<U, E> by applying a function to a contained Ok value, leaving an Err value untouched. This function can be used to compose the results of two functions.") {
        let closure = { $0 + 2 }

        let result = self.successResult.map(closure)
        XCTAssertTrue(result.isSuccess())
        XCTAssertEqual(-1, result.getSuccess()!)

        let error = self.errorResult.map(closure)
        XCTAssertTrue(error.isError())
      }

      // swiftlint:disable:next line_length
      it("has method mapError. Maps a Result<T, E> to Result<T, F> by applying a function to a contained Err value, leaving an Ok value untouched. This function can be used to pass through a successful result while handling an error.") {
        let closure = { $0 + " some text" }

        let result = self.successResult.mapError(closure)
        XCTAssertTrue(result.isSuccess())
        XCTAssertEqual(self.successResult.getSuccess()!, result.getSuccess()!)

        let error = self.errorResult.mapError(closure)
        XCTAssertTrue(error.isError())
        XCTAssertNotEqual(self.errorResult.getError()!, error.getError()!)
      }

      it("has method and. Returns res if the result is Ok, otherwise returns the Err value of self.") {
        let firstCaseExpected = StringResult.error("late error")
        let firstCaseX = IntResult.success(2)
        let firstCaseY = firstCaseExpected
        let firstCaseActual = firstCaseX.and(firstCaseY)
        XCTAssertTrue(firstCaseActual.isError())
        XCTAssertEqual(firstCaseExpected.getError()!, firstCaseActual.getError()!)

        let secondCaseExpected = IntResult.error("early error")
        let secondCaseX = secondCaseExpected
        let secondCaseY = StringResult.success("foo")
        let secondCaseActual = secondCaseX.and(secondCaseY)
        XCTAssertTrue(secondCaseActual.isError())
        XCTAssertEqual(secondCaseExpected.getError()!, secondCaseActual.getError()!)

        let thirdCaseExpected = IntResult.error("not a 2")
        let thirdCaseX = thirdCaseExpected
        let thirdCaseY = StringResult.error("late error")
        let thirdCaseActual = thirdCaseX.and(thirdCaseY)
        XCTAssertTrue(thirdCaseActual.isError())
        XCTAssertEqual(thirdCaseExpected.getError()!, thirdCaseActual.getError()!)

        let fourthCaseExpected = StringResult.success("different result type")
        let fourthCaseX = IntResult.success(2)
        let fourthCaseY = fourthCaseExpected
        let fourthCaseActual = fourthCaseX.and(fourthCaseY)
        XCTAssertTrue(fourthCaseActual.isSuccess())
        XCTAssertEqual(fourthCaseActual.getSuccess()!, fourthCaseExpected.getSuccess()!)
      }

      // swiftlint:disable:next line_length
      it("has method andThen. Calls op if the result is Ok, otherwise returns the Err value of self. This function can be used for control flow based on Result values.") {
        let square: (Int) -> Result<Int, Int> = { .success($0 * $0) }
        let error: (Int) -> Result<Int, Int> = { .error($0) }

        XCTAssertEqual(Result<Int, Int>.success(2).andThen(square).andThen(square).getSuccess()!, 16)
        // swiftlint:disable:next line_length
        XCTAssertEqual(Result<Int, Int>.success(2).andThen(square).andThen(error).getError()!, Result<Int, Int>.error(4).getError()!)
        // swiftlint:disable:next line_length
        XCTAssertEqual(Result<Int, Int>.success(2).andThen(error).andThen(square).getError()!, Result<Int, Int>.error(2).getError()!)
        // swiftlint:disable:next line_length
        XCTAssertEqual(Result<Int, Int>.error(3).andThen(square).andThen(square).getError()!, Result<Int, Int>.error(3).getError()!)
      }

      it("has method or. Returns res if the result is Err, otherwise returns the Ok value of self.") {
        let firseExpected = 2
        let firstCaseX = IntResult.success(firseExpected)
        let firstCaseY = IntResult.error("late error")
        let firstActual = firstCaseX.or(firstCaseY).getSuccess()!
        XCTAssertEqual(firseExpected, firstActual)

        let secondCaseExpected = 2
        let secondCaseX = IntResult.error("early error")
        let secondCaseY = IntResult.success(secondCaseExpected)
        let secondCaseActual = secondCaseX.or(secondCaseY).getSuccess()!
        XCTAssertEqual(secondCaseExpected, secondCaseActual)

        let thirdCaseExpected = "late error"
        let thirdCaseX = IntResult.error("not a 2")
        let thirdCaseY = IntResult.error(thirdCaseExpected)
        let thirdCaseActual = thirdCaseX.or(thirdCaseY).getError()!
        XCTAssertEqual(thirdCaseExpected, thirdCaseActual)

        let fourthCaseExpected = 2
        let fourthCaseX = IntResult.success(fourthCaseExpected)
        let fourthCaseY = IntResult.success(100)
        let fourthCaseActual = fourthCaseX.or(fourthCaseY).getSuccess()!
        XCTAssertEqual(fourthCaseExpected, fourthCaseActual)
      }

      // swiftlint:disable:next line_length
      it("has method orElse. Calls op if the result is Err, otherwise returns the Ok value of self. This function can be used for control flow based on result values.") {
        let square: (Int) -> Result<Int, Int> = { .success($0 * $0) }
        let error: (Int) -> Result<Int, Int> = { .error($0) }

        XCTAssertEqual(Result<Int, Int>.success(2).orElse(square).orElse(square).getSuccess()!, 2)
        XCTAssertEqual(Result<Int, Int>.success(2).orElse(error).orElse(square).getSuccess()!, 2)
        XCTAssertEqual(Result<Int, Int>.error(3).orElse(square).orElse(error).getSuccess()!, 9)
        XCTAssertEqual(Result<Int, Int>.error(3).orElse(error).orElse(error).getError()!, 3)
      }

      it("has method unwrapOr. Unwraps a result, yielding the content of an Ok. Else, it returns optb.") {
        let optb = 2
        let success = IntResult.success(9)
        XCTAssertEqual(success.unwrapOr(optb), 9)

        let error = IntResult.error("error")
        XCTAssertEqual(error.unwrapOr(optb), optb)
      }

      // swiftlint:disable:next line_length
      it("has method unwrapOrElse. Unwraps a result, yielding the content of an Ok. If the value is an Err then it calls op with its value.") {
        let count: (String) -> Int = { $0.characters.count }

        let success = IntResult.success(2)
        XCTAssertEqual(success.unwrapOrElse(count), 2)

        let error = IntResult.error("foo")
        XCTAssertEqual(error.unwrapOrElse(count), 3)
      }
    }
  }
}
