Access-Control-Allow-Origin: *
Cache-Control: public, max-age=60, s-maxage=60
Content-Encoding: gzip
Content-Type: application/json; charset=utf-8
Date: Sat, 31 Dec 2016 09:28:28 GMT
Etag: W/"1862fb3535367efe0d3bb168024f4f63"
Last-Modified: Thu, 22 Dec 2016 15:18:33 GMT
Server: GitHub.com
Status: 200 OK
Strict-Transport-Security: max-age=31536000; includeSubdomains; preload
Transfer-Encoding: chunked
Vary: Accept, Accept-Encoding
X-Content-Type-Options: nosniff
X-Frame-Options: deny
X-GitHub-Media-Type: github.v3; format=json
X-GitHub-Request-Id: D942988C:0B77:3E13FD:58677A3C
X-RateLimit-Limit: 60
X-RateLimit-Remaining: 42
X-RateLimit-Reset: 1483178629
X-Served-By: 9000e9eef7bb1e89f22030c676da140e
X-XSS-Protection: 1; mode=block
access-control-expose-headers: ETag, Link, X-GitHub-OTP, X-RateLimit-Limit, X-RateLimit-Remaining, X-RateLimit-Reset, X-OAuth-Scopes, X-Accepted-OAuth-Scopes, X-Poll-Interval
content-security-policy: default-src 'none'
