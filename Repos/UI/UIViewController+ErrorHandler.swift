//
//  UIViewController+ErrorHandler.swift
//  Repos
//
//  Created by funkill on 15/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import UIKit.UIViewController

extension UIViewController {

  internal func handleErrorDTO(_ error: ErrorDTO) {
    self.alertError(message: error.message)
  }

  internal func handleError(_ error: NSError) {
    self.alertError(message: error.domain)
  }

  private func alertError(message: String) {
    DispatchQueue.main.async {
      self.resignFirstResponder()
      let alert = ElementsFactory.createAlert(message: message)
      self.present(alert, animated: true)
    }
  }

}
