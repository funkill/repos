//
//  SearchTableDelegate.swift
//  Repos
//
//  Created by funkill on 29/11/16.
//
//

import UIKit

class SearchTableDataSource: NSObject, UITableViewDataSource {
  private let cellIdentitier = "searchVCCell"

  var data = [RepositorySearchItemDTO]()

  // MARK: - UITableViewDataSource
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return data.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    var cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentitier)
    if cell == nil {
      cell = ElementsFactory.createTableCell(self.cellIdentitier)
    }

    return indexPath.row < self.data.count
      ? self.configureCellBy(cell!, by: (self.data[indexPath.row]))
      : cell!
  }

  // MARK: - Private
  private func configureCellBy(_ cell: UITableViewCell, by item: RepositorySearchItemDTO) -> UITableViewCell {
    cell.detailTextLabel?.text = item.description
    cell.textLabel?.text = item.fullName
    cell.accessoryType = .disclosureIndicator

    return cell
  }

}
