//
//  SearchViewController.swift
//  Repos
//
//  Created by funkill on 29/11/16.
//
//

import UIKit

class SearchViewController: UIViewController, UITableViewDelegate, UISearchBarDelegate {

  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var searchBar: UISearchBar!

  private var lockSameSearch = false
  private let repositorySearcher = RepositorySearch()

  private let tableDataSource = SearchTableDataSource()

  let repoVC = RepositoryViewController()

  override func viewDidLoad() {
    self.tableView.delegate = self
    self.tableView.dataSource = self.tableDataSource
    self.searchBar.delegate = self
  }

  override func viewWillAppear(_ animated: Bool) {
    self.navigationController!.setNavigationBarHidden(true, animated: false)
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    // swiftlint:disable:next force_cast line_length
    let repoVC = self.storyboard!.instantiateViewController(withIdentifier: "repovc") as! RepositoryViewController

    let repositoryName = self.tableDataSource.data[indexPath.row].fullName ?? ""
    let repositoryDescription = self.tableDataSource.data[indexPath.row].description ?? ""

    var model = RepositoryUIModel()
    model.fullName = repositoryName
    model.description = repositoryDescription
    repoVC.model = model

    Favorites().isFavoriteRepositry(fullName: repositoryName, description: repositoryDescription) {
      result in
      DispatchQueue.main.async {
        if case let Result.success(_result) = result {
          repoVC.model!.favorite = _result
        }

        repoVC.updateModel()
      }
    }

    UserService().getUser(self.tableDataSource.data[indexPath.row].owner.login) {
      response in
      switch response {
        case let .success(value):
          DispatchQueue.main.async {
            // swiftlint:disable:next force_cast
            repoVC.model!.userName = (value as! UserDTO).login ?? ""
            // swiftlint:disable:next force_cast
            repoVC.model!.email = (value as! UserDTO).email ?? ""
            repoVC.model!.locked = false

            repoVC.updateModel()
          }

        case let .error(error):
          self.handleErrorDTO(error)

      }
    }

    self.navigationController!.pushViewController(repoVC, animated: true)
    self.navigationController!.setNavigationBarHidden(false, animated: true)

    tableView.deselectRow(at: indexPath, animated: false)
  }

  public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    if let query = searchBar.text {
      self.clearTable()

      repositorySearcher.searchBy(query, callback: self.updateTableData)
    }
  }

  public func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let currentOffset = scrollView.contentOffset.y
    if currentOffset <= 0 {
      return
    }

    let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
    let deltaOffset = maximumOffset - currentOffset

    if deltaOffset > 0 {
      return
    }

    if self.searchBar.text! == "" {
      return
    }

    if lockSameSearch {
      return
    }

    lockSameSearch = true

    repositorySearcher.getNextPage {
      result in
      self.updateTableData(result)
      self.tableView.sectionFooterHeight = 0
      self.lockSameSearch = false
    }
  }

  // MARK: - Private

  private func updateTableData(_ result: RepositorySearchResult) {
    switch result {
      case let .success(value):
        // swiftlint:disable:next force_cast
        let value = value as! RepositorySearchDTO
        let items = value.items!
        self.tableDataSource.data += items

        DispatchQueue.main.async {
          self.tableView.reloadData()
        }

      case let .error(error):
        self.handleErrorDTO(error)
    }
  }

  private func clearTable() {
    self.tableDataSource.data = []
    DispatchQueue.main.async {
      self.tableView.reloadData()
    }
  }

}
