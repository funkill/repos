//
//  FavoritesTableDataSource.swift
//  Repos
//
//  Created by funkill on 12/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import UIKit

class FavoritesTableDataSource: NSObject, UITableViewDataSource {

  private let cellIdentitier = "favoriteVC"
  var data: [FavoriteModel] = []

  // MARK: - UITableViewDataSource
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return data.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    var cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentitier)
    if cell == nil {
      cell = ElementsFactory.createTableCell(self.cellIdentitier)
    }

    return indexPath.row < self.data.count
      ? self.configureCellBy(cell!, by: (self.data[indexPath.row]))
      : cell!
  }

  // MARK: - Private
  private func configureCellBy(_ cell: UITableViewCell, by item: FavoriteModel) -> UITableViewCell {
    cell.detailTextLabel?.text = item.repository.repositoryDescription
    cell.textLabel?.text = item.repository.fullName
    cell.accessoryType = .disclosureIndicator

    return cell
  }

}
