//
//  FavoritesViewController.swift
//  Repos
//
//  Created by funkill on 29/11/16.
//
//

import UIKit

class FavoritesViewController: UIViewController, UITableViewDelegate {

  @IBOutlet weak var tableView: UITableView!

  private let tableDataSource = FavoritesTableDataSource()

  override func viewDidLoad() {
    super.viewDidLoad()
    self.tableView.dataSource = self.tableDataSource
    self.tableView.delegate = self
  }

  override func viewWillAppear(_ animated: Bool) {
    self.navigationController!.setNavigationBarHidden(true, animated: false)
    self.updateModel()
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    // swiftlint:disable:next force_cast line_length
    let repoVC = self.storyboard!.instantiateViewController(withIdentifier: "repovc") as! RepositoryViewController
    let repository = self.tableDataSource.data[indexPath.row]
    var model = RepositoryUIModel()
    model.fullName = repository.repository.fullName
    model.description = repository.repository.repositoryDescription
    model.email = repository.user.email
    model.userName = repository.user.name
    model.favorite = true
    model.locked = false
    repoVC.model = model

    DispatchQueue.main.async {
      repoVC.updateModel()
    }

    self.navigationController!.pushViewController(repoVC, animated: true)
    self.navigationController!.setNavigationBarHidden(false, animated: true)
    tableView.deselectRow(at: indexPath, animated: false)
  }

  private func updateModel() {
    Favorites().getAllFavorites {
      result in
      switch result {
        case let .success(data):
          DispatchQueue.main.async {
            self.tableDataSource.data = data
            self.tableView.reloadData()
          }

        case let .error(error):
          self.handleError(error as NSError)
      }
    }
  }

}
