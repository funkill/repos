//
//  RepositoryViewController.swift
//  Repos
//
//  Created by funkill on 25/12/16.
//
//

import UIKit

class RepositoryViewController: UITableViewController {
  var model: RepositoryUIModel?

  @IBOutlet weak var addFavoriteButton: UIButton!
  @IBOutlet weak var removeFavoriteButton: UIButton!

  @IBOutlet weak var name: UILabel!
  @IBOutlet weak var userName: UILabel!
  @IBOutlet weak var email: UILabel!
  @IBOutlet weak var repoDescription: UILabel!

  func updateModel() {
    self.name.text = self.model!.fullName
    self.userName.text = self.model!.userName
    self.email.text = self.model!.email
    self.repoDescription.text = self.model!.description

    self.changeButtons(isFavorite: self.model!.favorite, locked: self.model!.locked)
  }

  @IBAction func addFavorite(_ sender: Any) {
    let favorite = FavoriteModel(
      fullName: self.model!.fullName,
      repositoryDescription: self.model!.description,
      email: self.model!.email,
      name: self.model!.userName
    )

    Favorites().addFavorite(favorite) {
      result in

      switch result {
        case .success():
          DispatchQueue.main.async {
            self.model!.favorite = true
            self.changeButtons(isFavorite: self.model!.favorite, locked: self.model!.locked)
          }
        case let .error(error):
          self.handleError(error as NSError)
      }
    }
  }

  @IBAction func removeFavorite(_ sender: Any) {
    let favorite = FavoriteModel(
      fullName: self.model!.fullName,
      repositoryDescription: self.model!.description,
      email: self.model!.email,
      name: self.model!.userName
    )

    Favorites().removeFavorite(favorite) {
      result in

      switch result {
        case .success():
          DispatchQueue.main.async {
            self.model!.favorite = false
            self.changeButtons(isFavorite: self.model!.favorite, locked: self.model!.locked)
          }

        case let .error(error):
          self.handleError(error as NSError)
      }
    }
  }

  private func changeButtons(isFavorite: Bool, locked: Bool) {
    if locked {
      self.removeFavoriteButton.isEnabled = false
      self.addFavoriteButton.isEnabled = false

      return
    }

    self.removeFavoriteButton.isEnabled = isFavorite
    self.addFavoriteButton.isEnabled = !isFavorite
  }

}
