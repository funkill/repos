//
//  RepositoryUIModel.swift
//  Repos
//
//  Created by funkill on 27/12/16.
//
//

import Foundation

struct RepositoryUIModel {
  var fullName: String!
  var description: String!
  var userName: String!
  var email: String!
  var locked: Bool = true
  var favorite: Bool = false
}
