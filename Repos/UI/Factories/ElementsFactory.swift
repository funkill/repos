//
// Created by funkill on 13/12/16.
//

import UIKit

struct ElementsFactory {
  static func createTableCell(_ identifier: String) -> UITableViewCell {
    let cell = UITableViewCell(style: .subtitle, reuseIdentifier: identifier)

    return cell
  }

  static func createAlert(message: String) -> UIAlertController {
    let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
    let action = ElementsFactory.createOkAction()
    alert.addAction(action)

    return alert
  }

  static func createOkAction() -> UIAlertAction {
    return UIAlertAction(title: "Ok", style: .cancel)
  }
}
