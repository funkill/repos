//
//  SearchRepositoriesService.swift
//  Repos
//
//  Created by funkill on 29/11/16.
//
//

import Foundation
import UIKit.UIApplication

class SearchRepositoriesService: SearchService {

  static var dtoClass: DataTransferObject.Type {
    return RepositorySearchDTO.self
  }

  private static let path = "/search/repositories"

  public func searchBy(_ query: String, page: Int = 1, callback: @escaping (SearchResult) -> Void) {
    var request = Request()
    request.query = ["q": query, "page": page]

    self.restClient.get(SearchRepositoriesService.path, withRequest: request) {
      response in
      let result = self.prepareSearchResponse(response)
      callback(result)
    }
  }

}
