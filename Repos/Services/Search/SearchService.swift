//
//  SearchService.swift
//  Repos
//
//  Created by funkill on 21/12/16.
//
//

import Foundation

typealias SearchResult = Result<SearchResponse, ErrorDTO>

struct SearchResponse {
  let nextPage: Int?
  let previousPage: Int?
  let firstPage: Int?
  let lastPage: Int?
  let dto: DataTransferObject

  fileprivate init(dto: DataTransferObject, pages: Pages) {
    self.nextPage = pages.next
    self.previousPage = pages.previous
    self.firstPage = pages.first
    self.lastPage = pages.last
    self.dto = dto
  }
}

fileprivate struct Pages {
  var next: Int?
  var previous: Int?
  var first: Int?
  var last: Int?
}

protocol SearchService: NetworkService {
  func searchBy(_ query: String, page: Int, callback: @escaping (SearchResult) -> Void)
}

extension SearchService {
  func prepareSearchResponse(_ rawResponse: ResponseResult) -> SearchResult {
    let result = self.prepareResponse(rawResponse)
    switch result {
    case let .success(value):
      let pages = self.getPages(rawResponse.getSuccess()!)
      let searchResponse = SearchResponse(dto: value, pages: pages)

      return .success(searchResponse)

    case let .error(err):
      return .error(err)
    }
  }

  private func getPages(_ response: Response) -> Pages {
    let pages = Pages()
//    let links = response.headers!["Link"]! as! String
//    if let nextPage = self.getPageWithIdentifier(links, identifier: "next") {
//      pages.next = Int(nextPage, radix: 10)
//    }

    return pages
  }

  private func getPageWithIdentifier(_ links: String, identifier: String) -> String? {
    var regexp: NSRegularExpression
    do {
      // swiftlint:disable:next line_length
      regexp = try NSRegularExpression(pattern: "\\<[\\w\\.\\/\\?\\:]+page=(\\d+).*\\>; rel=\"\(identifier)\"", options: .caseInsensitive)
    } catch {
      return nil
    }

    let linksRange = NSRange(location: 0, length: links.characters.count)
    let matches = regexp.matches(in: links, range: linksRange)
    if matches.isEmpty {
      return nil
    }

    return nil
  }
}
