//
//  NetworkService.swift
//  Repos
//
//  Created by funkill on 29/11/16.
//
//

import Foundation

typealias NetworkServiceResult = Result<DataTransferObject, ErrorDTO>

protocol NetworkService: Service {
  static var dtoClass: DataTransferObject.Type { get }
}

extension NetworkService {

  // MARK: - Properties
  // MARK: • Instance

  internal var restClient: RestClient {
    return Registry.restClient
  }

  // MARK: - Base object methods
  final func prepareResponse(_ response: ResponseResult) -> NetworkServiceResult {
    switch response {
    case let .success(value) where value.code != 200:
      let jsonString = String(data: value.body ?? Data(), encoding: .utf8)!
      let errorDto = ErrorDTO.mapped(jsonString)
      Registry.logger.warning(
        "Response of request has error",
        context: [
          "Error": errorDto,
          "Response": value,
        ]
      )

      return .error(errorDto)

    case let .success(value):
      let jsonString = String(data: value.body ?? Data(), encoding: .utf8)!
      let dto = Self.dtoClass.mapped(jsonString)

      return .success(dto)

    case let .error(error):
      let errorDto = ErrorDTO(message: "Unknown error", errors: [])
      Registry.logger.error(
        "Request falls with error",
        context: [
          "Error": errorDto,
          "Response": error,
        ]
      )

      return .error(errorDto)
    }
  }

}
