//
// Created by funkill on 06/01/17.
// Copyright (c) 2017 E-legion. All rights reserved.
//

import Foundation

protocol Service {}

typealias SimpleServiceResult = Result<Void, Error>
typealias SimpleServiceClosure = (SimpleServiceResult) -> Void
