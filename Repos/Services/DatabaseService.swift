//
//  DatabaseService.swift
//  Repos
//
//  Created by funkill on 06/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation

protocol DatabaseService: Service {
}

extension DatabaseService {
  internal var manager: DBManager {
    return Registry.dbManager
  }
}
