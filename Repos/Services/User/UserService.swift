//
// Created by funkill on 26/12/16.
//

import Foundation

class UserService: NetworkService {

  // MARK: - Properties
  // MARK: • Class & static
  class var dtoClass: DataTransferObject.Type {
    return UserDTO.self
  }

  private static let path = "/users/"

  // MARK: - Methods
  func getUser(_ name: String, callback: @escaping (NetworkServiceResult) -> Void) {
    let resultPath = self.preparePathWith(name)
    self.restClient.get(resultPath) {
      rawResponse in

      let response = self.prepareResponse(rawResponse)
      callback(response)
    }
  }

  // MARK: • Private
  private func preparePathWith(_ name: String) -> String {
    return UserService.path + name
  }

}
