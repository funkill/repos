//
// Created by funkill on 06/01/17.
// Copyright (c) 2017 E-legion. All rights reserved.
//

import CoreData

final class FavoritesService: DatabaseService {

  func addFavorite(_ favorite: FavoriteModel, closure: @escaping SimpleServiceClosure) {
    self.manager.transaction({
      context in
      // in this case is better use repository
      var user = try Users.find(context: context, name: favorite.user.name, email: favorite.user.email).first
      if user == nil {
        user = Users(context: context)
        user!.name = favorite.user.name
        user!.email = favorite.user.email
        user!.save()
      }

      let repo = Repositories(context: context)
      repo.repository_user = user!
      repo.fullName = favorite.repository.fullName
      repo.repositoryDescription = favorite.repository.repositoryDescription
      repo.save()
    }, after: { closure($0) })
  }

  func removeFavorite(_ favorite: FavoriteModel, closure: @escaping SimpleServiceClosure) {
    self.manager.perform({
      context in
      let repo = Repositories.find(context: self.manager.mainContext, name: favorite.repository.fullName).first
      if repo == nil {
        throw NSError(domain: "Repo not found", code: -1)
      }

      repo!.remove()
    }, after: { closure($0) })
  }

  func getAllFavorites(_ closure: @escaping FavoritesClosure) {
    self.manager.perform({
      context in

      let repos = Repositories
        .find(context: context)
        .map {
          repo in
          return FavoriteModel(
            fullName: repo.fullName ?? "",
            repositoryDescription: repo.repositoryDescription ?? "",
            email: repo.repository_user.email ?? "",
            name: repo.repository_user.name ?? ""
          )
        }

      closure(Result.success(repos))
    }, after: { _ in })
  }

  func isFavoriteRepositry(fullName: String, description: String, closure: @escaping (Result<Bool, Error>) -> Void) {
    self.manager.perform({
      context in
      let isExist = Repositories.exists(context: self.manager.mainContext, name: fullName)
      closure(Result.success(isExist))
    }, after: {
      _ in

    })
  }
}
