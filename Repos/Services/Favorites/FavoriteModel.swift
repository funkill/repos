//
//  FavoriteModel.swift
//  Repos
//
//  Created by funkill on 06/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation

struct FavoriteModel {
  let repository: RepositoryModel
  let user: UserModel

  init(fullName: String, repositoryDescription: String, email: String, name: String) {
    self.repository = RepositoryModel(fullName: fullName, repositoryDescription: repositoryDescription)
    self.user = UserModel(email: email, name: name)
  }
}

struct RepositoryModel {
  var fullName: String
  var repositoryDescription: String
}

struct UserModel {
  var email: String
  var name: String
}
