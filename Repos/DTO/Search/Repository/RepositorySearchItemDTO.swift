//
//  RepositorySearchItemDTO.swift
//  Repos
//
//  Created by funkill on 10/12/16.
//
//

import Foundation
import ObjectMapper

struct RepositorySearchItemDTO: DataTransferObject, StaticMappable {
  var id: Int!
  var name: String!
  var fullName: String!
  var owner: OwnerDTO!
  var `private`: Bool!
  var htmlUrl: String!
  var description: String!
  var fork: Bool!
  var url: String!
  var createdAt: Date!
  var updatedAt: Date!
  var pushedAt: Date!
  var homepage: String!
  var size: Int!
  var stargazersCount: Int!
  var watchersCount: Int!
  var language: String!
  var forksCount: Int!
  var openIssuesCount: Int!
  var masterBranch: Int!
  var defaultBranch: Int!
  var score: Double!

  static func mapped(_ json: String) -> RepositorySearchItemDTO {
    return RepositorySearchItemDTO(JSONString: json)!
  }

  static func objectForMapping(map: ObjectMapper.Map) -> BaseMappable? {
    return RepositorySearchItemDTO()
  }

  mutating func mapping(map: Map) {
    id <- map["id"]
    name <- map["name"]
    fullName <- map["full_name"]
    owner <- map["owner"]
    `private` <- map["private"]
    htmlUrl <- map["html_url"]
    description <- map["description"]
    fork <- map["fork"]
    url <- map["url"]
    createdAt <- (map["created_at"], DateTransform())
    updatedAt <- (map["updated_at"], DateTransform())
    pushedAt <- (map["pushed_at"], DateTransform())
    homepage <- map["homepage"]
    size <- map["size"]
    stargazersCount <- map["stargazers_count"]
    watchersCount <- map["watchers_count"]
    language <- map["language"]
    forksCount <- map["forks_count"]
    openIssuesCount <- map["open_issues_count"]
    masterBranch <- map["master_branch"]
    defaultBranch <- map["default_branch"]
    score <- map["score"]
  }

}
