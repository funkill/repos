//
//  RepositorySearchDTO.swift
//  Repos
//
//  Created by funkill on 29/11/16.
//
//

import Foundation
import ObjectMapper

struct RepositorySearchDTO: DataTransferObject, StaticMappable {
  var totalCount: Int!
  var incompleteResults: Bool!
  var items: [RepositorySearchItemDTO]!

  static func mapped(_ json: String) -> RepositorySearchDTO {
    return RepositorySearchDTO(JSONString: json)!
  }

  static func objectForMapping(map: ObjectMapper.Map) -> BaseMappable? {
    return RepositorySearchDTO()
  }

  mutating func mapping(map: Map) {
    totalCount <- map["total_count"]
    incompleteResults <- map["incomplete_results"]
    items <- map["items"]
  }
}
