//
//  UserDTO.swift
//  Repos
//
//  Created by funkill on 26/12/16.
//
//

import Foundation
import ObjectMapper

struct UserDTO: DataTransferObject, StaticMappable {
  var login: String!
  var id: Int!
  var avatarUrl: String!
  var gravatarId: String!
  var url: String!
  var htmlUrl: String!
  var followersUrl: String!
  var followingUrl: String!
  var gistsUrl: String!
  var starredUrl: String!
  var subscriptionsUrl: String!
  var organizationsUrl: String!
  var reposUrl: String!
  var eventsUrl: String!
  var receivedEventsUrl: String!
  var type: String!
  var siteAdmin: Bool!
  var name: String!
  var company: String!
  var blog: String!
  var location: String!
  var email: String!
  var hireable: Bool!
  var bio: String!
  var publicRepos: Int!
  var publicGists: Int!
  var followers: Int!
  var following: Int!
  var createdAt: Date!
  var updatedAt: Date!

  static func mapped(_ json: String) -> UserDTO {
    return UserDTO(JSONString: json)!
  }

  static func objectForMapping(map: ObjectMapper.Map) -> BaseMappable? {
    return UserDTO()
  }

  mutating func mapping(map: Map) {
    login <- map["login"]
    id <- map["id"]
    avatarUrl <- map["avatar_url"]
    gravatarId <- map["gravatar_id"]
    url <- map["url"]
    htmlUrl <- map["html_url"]
    followersUrl <- map["followers_url"]
    followingUrl <- map["following_url"]
    gistsUrl <- map["gists_url"]
    starredUrl <- map["starred_url"]
    subscriptionsUrl <- map["subscriptions_url"]
    organizationsUrl <- map["organizations_url"]
    reposUrl <- map["repos_url"]
    eventsUrl <- map["events_url"]
    receivedEventsUrl <- map["received_events_url"]
    type <- map["type"]
    siteAdmin <- map["site_admin"]
    name <- map["name"]
    company <- map["company"]
    blog <- map["blog"]
    location <- map["location"]
    email <- map["email"]
    hireable <- map["hireable"]
    bio <- map["bio"]
    publicRepos <- map["public_repos"]
    publicGists <- map["public_gists"]
    followers <- map["followers"]
    following <- map["following"]
    createdAt <- (map["created_at"], DateTransform())
    updatedAt <- (map["updated_at"], DateTransform())
  }
}
