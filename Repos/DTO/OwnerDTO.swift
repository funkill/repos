//
//  OwnerDTO.swift
//  Repos
//
//  Created by funkill on 10/12/16.
//
//

import Foundation
import ObjectMapper

struct OwnerDTO: DataTransferObject, StaticMappable {
  var login: String!
  var id: Int!
  var avatarUrl: String!
  var gravatarId: String!
  var url: String!
  var receivedEventsUrl: String!
  var type: String!

  static func mapped(_ json: String) -> OwnerDTO {
    return OwnerDTO(JSONString: json)!
  }

  static func objectForMapping(map: ObjectMapper.Map) -> BaseMappable? {
    return OwnerDTO()
  }

  mutating func mapping(map: Map) {
    login <- map["login"]
    id <- map["id"]
    avatarUrl <- map["avatar_url"]
    gravatarId <- map["gravatar_id"]
    url <- map["url"]
    receivedEventsUrl <- map["received_events_url"]
    type <- map["type"]
  }

}
