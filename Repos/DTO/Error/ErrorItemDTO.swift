//
//  ErrorItemDTO.swift
//  Repos
//
//  Created by funkill on 12/12/16.
//
//

import Foundation
import ObjectMapper

enum ErrorItemCodes: String {
  case Missing = "missing"
  case MissingField = "missing_field"
  case Invalid = "invalid"
  case AlreadyExists = "already_exists"
}

struct ErrorItemDTO: StaticMappable {

  var resource: String!
  var field: String!
  var code: ErrorItemCodes?
  var documentationUrl: String?

  static func objectForMapping(map: ObjectMapper.Map) -> BaseMappable? {
    return ErrorItemDTO()
  }

  mutating func mapping(map: ObjectMapper.Map) {
    resource <- map["resource"]
    field <- map["field"]
    code <- map["code"]
    documentationUrl <- map["documentation_url"]
  }

  var debugDescription: String {
    return "Resource: \(self.resource)\n"
      + "Field: \(self.field)\n"
      + "Code: \(self.code)\n"
      + "Documentation: \(self.documentationUrl)"
  }
}
