//
//  ErrorDTO.swift
//  Repos
//
//  Created by funkill on 12/12/16.
//
//

import Foundation
import ObjectMapper

struct ErrorDTO: DataTransferObject, StaticMappable {
  var message: String!
  var errors: [ErrorItemDTO]?

  static func mapped(_ json: String) -> ErrorDTO {
    return ErrorDTO(JSONString: json) ?? ErrorDTO(message: "Unknown error", errors: nil)
  }

  static func objectForMapping(map: ObjectMapper.Map) -> BaseMappable? {
    return ErrorDTO()
  }

  mutating func mapping(map: ObjectMapper.Map) {
    message <- map["message"]
    errors <- map["errors"]
  }
}
