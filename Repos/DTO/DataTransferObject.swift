//
//  DataTransferObject.swift
//  Repos
//
//  Created by funkill on 10/12/16.
//
//

import Foundation

protocol DataTransferObject {
  static func mapped(_ json: String) -> Self
}
