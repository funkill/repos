//
//  Favorites.swift
//  Repos
//
//  Created by funkill on 06/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation

// swiftlint:disable:next syntactic_sugar
typealias FavoritesResult = Result<Array<FavoriteModel>, Error>
typealias FavoritesClosure = (FavoritesResult) -> Void

class Favorites {

  private lazy var service: FavoritesService = {
    return FavoritesService()
  }()

  func addFavorite(_ favorite: FavoriteModel, closure: @escaping SimpleServiceClosure) {
    self.service.addFavorite(favorite) { closure($0) }
  }

  func removeFavorite(_ favorite: FavoriteModel, closure: @escaping SimpleServiceClosure) {
    self.service.removeFavorite(favorite) { closure($0) }
  }

  func getAllFavorites(closure: @escaping FavoritesClosure) {
    self.service.getAllFavorites(closure)
  }

  func isFavoriteRepositry(fullName: String, description: String, closure: @escaping (Result<Bool, Error>) -> Void) {
    self.service.isFavoriteRepositry(fullName: fullName, description: description) { closure($0) }
  }

}
