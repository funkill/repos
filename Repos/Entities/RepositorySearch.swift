//
//  RepositorySearch.swift
//  Repos
//
//  Created by funkill on 21/12/16.
//
//

import Foundation

typealias RepositorySearchResult = Result<DataTransferObject, ErrorDTO>
typealias RepositorySearchCallback = (RepositorySearchResult) -> Void

class RepositorySearch {

  private var page = 1
  private var query = ""
  private var language = ""
  private var hasNextPage = false
  private var isCompleteSearch = false
  private let service = SearchRepositoriesService()

  func searchBy(_ query: String, callback: @escaping RepositorySearchCallback) {
    self.query = query
    self.page = 1
    self.isCompleteSearch = false
    self.getCurrentPage(callback: callback)
  }

  func getNextPage(callback: @escaping RepositorySearchCallback) {
    if self.hasNextPage {
      self.page += 1
      self.getCurrentPage(callback: callback)
    } else {
      callback(.error(ErrorDTO(message: "Next page not available", errors: [])))
    }
  }

  func getCurrentPage(callback: @escaping RepositorySearchCallback) {
    service.searchBy(self.query, page: self.page) { result in
      switch result {
        case let .success(response):
          self.hasNextPage = true //response.nextPage != nil
          callback(.success(response.dto))

        case let .error(error):
          callback(.error(error))
      }
    }
  }

}
