//
//  Result.swift
//  Repos
//
//  Created by funkill on 30/11/16.
//
//

import Foundation

// Аналог Maybe monade из Haskell (на самом деле, Optional Types более близки к Maybe,
// а если быть совсем точным, то данный енам взят из Rust
// https://doc.rust-lang.org/src/core/up/src/libcore/result.rs.html#260-268)
// https://hackage.haskell.org/package/base-4.9.0.0/docs/Data-Maybe.html
enum Result<T, E> {

  case success(T)
  case error(E)

  func isSuccess() -> Bool {
    switch self {
      case .success(_): return true
      case .error(_): return false
    }
  }

  func isError() -> Bool {
    return !self.isSuccess()
  }

  func getSuccess() -> T? {
    switch self {
      case let .success(value): return value
      case .error(_): return nil
    }
  }

  func getError() -> E? {
    switch self {
      case .success(_): return nil
      case let .error(error): return error
    }
  }

  func map<U>(_ closure: (T) -> U) -> Result<U, E> {
    switch self {
      case let .success(value): return .success(closure(value))
      case let .error(error): return .error(error)
    }
  }

  func mapError<U>(_ closure: (E) -> U) -> Result<T, U> {
    switch self {
      case let .success(value): return .success(value)
      case let .error(error): return .error(closure(error))
    }
  }

  func and<U>(_ other: Result<U, E>) -> Result<U, E> {
    switch self {
      case .success(_): return other
      case let .error(error): return .error(error)
    }
  }

  func andThen<U>(_ closure: (T) -> Result<U, E>) -> Result<U, E> {
    switch self {
      case let .success(value): return closure(value)
      case let .error(error): return .error(error)
    }
  }

  func or<F>(_ result: Result<T, F>) -> Result<T, F> {
    switch self {
      case let .success(value): return .success(value)
      case .error(_): return result
    }
  }

  func orElse<F>(_ closure: (E) -> Result<T, F>) -> Result<T, F> {
    switch self {
      case let .success(value): return .success(value)
      case let .error(error): return closure(error)
    }
  }

  func unwrapOr(_ orValue: T) -> T {
    switch self {
      case let .success(value): return value
      case .error(_): return orValue
    }
  }

  func unwrapOrElse(_ closure: (E) -> T) -> T {
    switch self {
      case let .success(value): return value
      case let .error(error): return closure(error)
    }
  }

}
