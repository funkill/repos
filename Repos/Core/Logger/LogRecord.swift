//
//  LogRecord.swift
//  Repos
//
//  Created by funkill on 11/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation

struct LogRecord {
  let time = Date()
  let message: String
  let channel: String
  let level: LogLevel
  let context: [String: Any]

  init(message: String, level: LogLevel, channel: String, context: [String: Any] = [:]) {
    self.message = message
    self.level = level
    self.channel = channel
    self.context = context
  }
}
