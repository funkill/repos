//
//  LogLevels.swift
//  Repos
//
//  Created by funkill on 11/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation

/// @see https://tools.ietf.org/html/rfc5424#section-6.2.1 Table 2. Syslog Message Severities
enum LogLevel: Int {

  case emergency = 0
  case alert = 1
  case critical = 2
  case error = 3
  case warning = 4
  case notice = 5
  case info = 6
  case debug = 7

  var name: String {
    switch self {
      case .emergency: return "Emergency"
      case .alert: return "Alert"
      case .critical: return "Critical"
      case .error: return "Error"
      case .warning: return "Warning"
      case .notice: return "Notice"
      case .info: return "Info"
      case .debug: return "Debug"
    }
  }

}

extension LogLevel: Comparable {

  public static func == (lhs: LogLevel, rhs: LogLevel) -> Bool {
    return lhs.rawValue == rhs.rawValue
  }

  public static func < (lhs: LogLevel, rhs: LogLevel) -> Bool {
    return lhs.rawValue < rhs.rawValue
  }

  public static func <= (lhs: LogLevel, rhs: LogLevel) -> Bool {
    return lhs.rawValue <= rhs.rawValue
  }

  public static func >= (lhs: LogLevel, rhs: LogLevel) -> Bool {
    return lhs.rawValue >= rhs.rawValue
  }

  public static func > (lhs: LogLevel, rhs: LogLevel) -> Bool {
    return lhs.rawValue > rhs.rawValue
  }

}
