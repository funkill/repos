//
//  ConsoleHandler.swift
//  Repos
//
//  Created by funkill on 11/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation

class ConsoleHandler: LogHandler {
  private let level: LogLevel

  var formatter: LogFormatter = DefaultFormatter()

  init(level: LogLevel = .warning) {
    self.level = level
  }

  func handle(_ record: LogRecord) -> Bool {
    let logString = self.formatter.format(record)
    self.write(logString)

    return true
  }

  func canHandle(level: LogLevel) -> Bool {
    return level < self.level
  }

  private func write(_ logString: String) {
    print(logString)
  }
}
