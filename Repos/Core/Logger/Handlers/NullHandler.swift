//
//  NullHandler.swift
//  Repos
//
//  Created by funkill on 12/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation

class NullHandler: LogHandler {
  private let level: LogLevel

  init(level: LogLevel = .warning) {
    self.level = level
  }

  func handle(_ record: LogRecord) -> Bool {
    return true
  }

  func canHandle(level: LogLevel) -> Bool {
    return level < self.level
  }
}
