//
// Created by funkill on 11/01/17.
// Copyright (c) 2017 E-legion. All rights reserved.
//

import Foundation

protocol LogHandler {
  func handle(_ record: LogRecord) -> Bool
  func canHandle(level: LogLevel) -> Bool
}
