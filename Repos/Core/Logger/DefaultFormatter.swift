//
//  DefaultFormatter.swift
//  Repos
//
//  Created by funkill on 11/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation

class DefaultFormatter: LogFormatter {
  private lazy var dateFormatter: DateFormatter = {
    var formatter = DateFormatter()
    formatter.calendar = Calendar.current
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"

    return formatter
  }()

  func format(_ record: LogRecord) -> String {
    let levelName = record.level.name
    let preparedContent = record.context.map({
      (key, value) in
        let stringifiedValue = String(describing: value)
        return "\(key): \(stringifiedValue)"
      })
      .joined(separator: ", ")
    let formattedTime = self.dateFormatter.string(from: record.time)

    return "\(formattedTime): \(record.channel) [\(levelName)]: \(record.message) {\(preparedContent)}"
  }

}
