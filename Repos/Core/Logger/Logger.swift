//
// Created by funkill on 08/01/17.
// Copyright (c) 2017 E-legion. All rights reserved.
//

import Foundation

protocol Logger {
  func emergency(_ message: String, context: [String: Any]?)
  func alert(_ message: String, context: [String: Any]?)
  func critical(_ message: String, context: [String: Any]?)
  func error(_ message: String, context: [String: Any]?)
  func warning(_ message: String, context: [String: Any]?)
  func notice(_ message: String, context: [String: Any]?)
  func info(_ message: String, context: [String: Any]?)
  func debug(_ message: String, context: [String: Any]?)
  func log(level: LogLevel, message: String, context: [String: Any]?)
}

extension Logger {

  func emergency(_ message: String, context: [String: Any]? = nil) {
    self.log(level: .emergency, message: message, context: context)
  }

  func alert(_ message: String, context: [String: Any]? = nil) {
    self.log(level: .alert, message: message, context: context)
  }

  func critical(_ message: String, context: [String: Any]? = nil) {
    self.log(level: .critical, message: message, context: context)
  }

  func error(_ message: String, context: [String: Any]? = nil) {
    self.log(level: .error, message: message, context: context)
  }

  func warning(_ message: String, context: [String: Any]? = nil) {
    self.log(level: .warning, message: message, context: context)
  }

  func notice(_ message: String, context: [String: Any]? = nil) {
    self.log(level: .notice, message: message, context: context)
  }

  func info(_ message: String, context: [String: Any]? = nil) {
    self.log(level: .info, message: message, context: context)
  }

  func debug(_ message: String, context: [String: Any]? = nil) {
    self.log(level: .debug, message: message, context: context)
  }

}
