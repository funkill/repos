//
//  Journalist.swift
//  Repos
//
//  Created by funkill on 11/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation

class Journalist: Logger {

  private let name: String
  private var handlers: [LogHandler]

  init(named name: String, handled handlers: [LogHandler] = []) {
    self.name = name
    self.handlers = handlers
  }

  func log(level: LogLevel, message: String, context: [String: Any]? = nil) {
    let record = self.createRecord(level: level, message: message, context: context ?? [:])
    self.pushRecord(record)
  }

  private func createRecord(level: LogLevel, message: String, context: [String: Any]) -> LogRecord {
    let record = LogRecord(message: message, level: level, channel: self.name, context: context)

    return record
  }

  private func pushRecord(_ record: LogRecord) {
    handlers.forEach {
      handler in
      if !handler.canHandle(level: record.level) {
        return
      }

      /** @todo: handle result of logging */
      let _ = handler.handle(record)
    }
  }

}
