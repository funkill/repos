//
//  LogFormatter.swift
//  Repos
//
//  Created by funkill on 11/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation

protocol LogFormatter {
  func format(_ record: LogRecord) -> String
}
