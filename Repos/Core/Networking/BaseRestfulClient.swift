//
// Created by funkill on 01/01/17.
// Copyright (c) 2017 E-legion. All rights reserved.
//

import Foundation

protocol BaseRestfulClient {
  func get(_ path: String, withRequest request: Request, callback: @escaping ResponseCallback)
}
