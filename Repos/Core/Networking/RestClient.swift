//
//  RestClient.swift
//  Repos
//
//  Created by funkill on 27/11/16.
//
//

import Foundation

class RestClient: BaseRestfulClient {

  private let client: RestfulClient

  internal init(_ client: RestfulClient) {
    self.client = client
  }

  func get(_ path: String, withRequest request: Request = Request(), callback: @escaping ResponseCallback) {
    self.client.get(path, withRequest: request, callback: callback)
  }
}
