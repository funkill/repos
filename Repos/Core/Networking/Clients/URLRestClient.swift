//
//  URLRestClient.swift
//  Repos
//
//  Created by funkill on 27/11/16.
//
//

import Foundation

class URLRestClient: RestfulClient {

  // MARK: - Properties

  // MARK: • Instance

  internal var logger: Logger?

  private let baseUrl: NSURL
  private let session: URLSession

  // MARK: - Lifecycle

  required init(baseUrl: String, logger: Logger? = nil) {
    self.baseUrl = NSURL(string: baseUrl)!
    let queue = OperationQueue()
    self.session = URLSession(configuration: .default, delegate: nil, delegateQueue: queue)

    self.logger = logger
  }

  // MARK: - Methods

  func get(_ url: String, withRequest request: Request, callback: @escaping ResponseCallback) {
    let request = self.createGetRequest(url: url, request: request)

    session
      .dataTask(with: request, completionHandler: {
        (data, urlResponse, error) in
        if error != nil {
          self.logger?.error("Data task falls with error", context: ["Error": error!, "Request": request])
          callback(ResponseResult.error(error!))

          return
        }

        // swiftlint:disable:next force_cast
        let httpResponse = urlResponse as! HTTPURLResponse
        let response = Response(
          code: httpResponse.statusCode,
          headers: httpResponse.allHeaderFields,
          body: data
        )

        callback(ResponseResult.success(response))
      })
      .resume()
  }

  // MARK: • Private

  private func createGetRequest(url: String, request: Request) -> URLRequest {
    let query = self.convertToQuery(request.query)
    let url = NSURL(string: url + "?" + query, relativeTo: self.baseUrl as URL)!
    var request = self.createRequestFor(url as URL, withData: request)
    request.httpMethod = "GET"
    self.logger?.info("Created GET request for url \(url)", context: ["Request": request])

    return request
  }

  private func convertToQuery(_ data: [String: Any]) -> String {
    return data
      .map({
        (key, value) in
        // swiftlint:disable:next line_length
        let encodedValue = String(describing: value).addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? ""

        return "\(key)=\(encodedValue)"
      })
      .joined(separator: "&")
  }

  private func createRequestFor(_ url: URL, withData data: Request) -> URLRequest {
    var request = URLRequest(url: url)
    request.allHTTPHeaderFields = data.headers

    return request
  }

}
