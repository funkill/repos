//
//  FileRestClient.swift
//  Repos
//
//  Created by funkill on 17/12/16.
//
//

import Foundation

class FileRestClient: RestfulClient {

  // MARK: - Properties

  // MARK: • Class & static

  let baseUrl: String
  let session: URLSession

  // MARK: - Lifecycle

  required init(baseUrl: String, logger: Logger? = nil) {
    self.baseUrl = baseUrl
    self.session = URLSession(configuration: .default)
  }

  // MARK: - Methods

  func get(_ path: String, withRequest request: Request, callback: @escaping ResponseCallback) {
    let url = self.preparePathForGetRequest(path)
    let request = self.createRequest(url)

    session
      .dataTask(with: request, completionHandler: { (data, urlResponse, error) in
        if error != nil {
          callback(ResponseResult.error(error!))

          return
        }

        let response = Response(code: 200, headers: [:], body: data)

        callback(ResponseResult.success(response))
      })
      .resume()
  }

  // MARK: • Private

  private func preparePathForGetRequest(_ path: String) -> URL {
    let path = path + "-get"

    return self.preparePath(path)
  }

  private func preparePath(_ path: String) -> URL {
    let path = self.baseUrl + "/Stubs/" + path + ".json"

    return URL(fileURLWithPath: path)
  }

  private func createRequest(_ url: URL) -> URLRequest {
    return URLRequest(url: url)
  }

}
