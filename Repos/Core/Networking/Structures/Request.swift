//
//  Request.swift
//  Repos
//
//  Created by funkill on 30/11/16.
//
//

import Foundation

struct Request {
  var body = [String: Any]()
  var query = [String: Any]()
  var headers = [String: String]()
}
