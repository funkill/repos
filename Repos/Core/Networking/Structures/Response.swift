//
//  Response.swift
//  Repos
//
//  Created by funkill on 27/11/16.
//
//

import Foundation

struct Response {
  let code: Int
  let headers: [AnyHashable: Any]?
  let body: Data?
}
