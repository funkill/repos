//
//  RestClientConfiguration.swift
//  Repos
//
//  Created by funkill on 01/12/16.
//
//

import Foundation

struct RestClientConfiguration {
  let baseUrl: String
  let client: RestfulClient.Type

  init(baseUrl: String, client: RestfulClient.Type) {
    self.baseUrl = baseUrl
    self.client = client
  }

  static var defaultConfiguration: RestClientConfiguration = {
    let baseUrl = "https://api.github.com/"
    let client = URLRestClient.self

    return RestClientConfiguration(baseUrl: baseUrl, client: client)
  }()

  static var fileConfiguration: RestClientConfiguration = {
    let baseUrl = Bundle.main.bundlePath
    let client = FileRestClient.self

    return RestClientConfiguration(baseUrl: baseUrl, client: client)
  }()
}
