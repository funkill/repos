//
//  RestfulClient.swift
//  Repos
//
//  Created by funkill on 27/11/16.
//
//

import Foundation

typealias ResponseResult = Result<Response, Error>
typealias ResponseCallback = (ResponseResult) -> Void

protocol RestfulClient: BaseRestfulClient {
  init(baseUrl: String, logger: Logger?)
}
