//
//  DBManager.swift
//  Repos
//
//  Created by funkill on 06/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation
import CoreData.NSManagedObjectContext

// Because i don't know realm, using of plain SQL (i know DDL, DML and DCL,
// but i don't know how it uses in iOS), i create concrete DBManager for Core Data stack.
// We can use different connections, we can change realization of connector,
// change persistent coordinator and other
class DBManager {

  var mainContext: NSManagedObjectContext {
    return self.connection.mainContext
  }

  private let connection: DBConnector

  init(_ connection: DBConnector) {
    self.connection = connection
  }

  func synchronize() {
    self.connection.synchronize()
  }

  func transaction(_ action: @escaping AtomicOperation, after closure: @escaping SimpleServiceClosure) {
    self.connection.transaction(action, after: closure)
  }

  func perform(_ action: @escaping AtomicOperation, after closure: @escaping SimpleServiceClosure) {
    self.connection.perform(action, after: closure)
  }
}
