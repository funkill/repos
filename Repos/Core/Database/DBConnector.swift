//
// Created by funkill on 06/01/17.
// Copyright (c) 2017 E-legion. All rights reserved.
//

import Foundation
import CoreData.NSManagedObjectContext

typealias AtomicOperation = (NSManagedObjectContext) throws -> Void
typealias DBOperation = AtomicOperation

protocol DBConnector {
  var mainContext: NSManagedObjectContext { get }

  func transaction(_ action: @escaping AtomicOperation, after closure: @escaping SimpleServiceClosure)
  func perform(_ action: @escaping AtomicOperation, after closure: @escaping SimpleServiceClosure)
  func synchronize()
}
