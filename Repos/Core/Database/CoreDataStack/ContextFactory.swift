//
//  ContextFactory.swift
//  Repos
//
//  Created by funkill on 07/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation
import CoreData.NSManagedObjectContext

class ContextFactory {
  static func newContext() -> NSManagedObjectContext {
    let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)

    return context
  }
}
