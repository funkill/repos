//
//  CoreDataConnector.swift
//  Repos
//
//  Created by funkill on 06/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation
import CoreData

class CoreDataConnector {

  lazy var applicationDocumentsDirectory: URL = {
    let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return urls.first!
  }()

  lazy var managedObjectModel: NSManagedObjectModel = {
    let modelURL = Bundle.main.url(forResource: "Repos", withExtension: "momd")!
    return NSManagedObjectModel(contentsOf: modelURL)!
  }()

  lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
    let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
    let url = self.applicationDocumentsDirectory.appendingPathComponent("Repos.sqlite")
    var failureReason = "There was an error creating or loading the application's saved data."
    do {
      // swiftlint:disable:next line_length
      try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
    } catch {
      // Report any error we got.
      var dict = [String: AnyObject]()
      dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
      dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?

      dict[NSUnderlyingErrorKey] = error as NSError
      let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
      NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
      abort()
    }

    return coordinator
  }()

  private(set) lazy var rootContext: NSManagedObjectContext = {
    let context = ContextFactory.newContext()
    context.persistentStoreCoordinator = self.persistentStoreCoordinator

    return context
  }()

}

extension CoreDataConnector: DBConnector {

  var mainContext: NSManagedObjectContext {
    return self.rootContext
  }

  func synchronize() {
    if rootContext.hasChanges {
      do {
        try rootContext.save()
      } catch {
        let nserror = error as NSError
        NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
        abort()
      }
    }
  }

  func transaction(_ action: @escaping AtomicOperation, after closure: @escaping SimpleServiceClosure) {
    let parentContext = self.rootContext
    let childContext = ContextFactory.newContext()
    childContext.parent = parentContext

    let unitOfWork = CoreDataUnitOfWork(parentContext: parentContext, currentContext: childContext)
    unitOfWork.execute(action) {
      result in

      switch result {
        case .success():
          closure(SimpleServiceResult.success())

        case let .error(error):
          Registry.logger.error("Error on transaction", context: ["Error": error])
          closure(SimpleServiceResult.error(error.unwrap()))
      }
    }
  }

  func perform(_ action: @escaping DBOperation, after closure: @escaping SimpleServiceClosure) {
    self.rootContext.perform {
      do {
        try action(self.rootContext)
        closure(SimpleServiceResult.success())
      } catch let error {
        Registry.logger.error("Error on action", context: ["Error": error])
        closure(SimpleServiceResult.error(error))
      }
    }
  }
}
