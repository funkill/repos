//
//  UnitOfWork.swift
//  Repos
//
//  Created by funkill on 07/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation
import UIKit.UIApplication
import CoreData.NSManagedObjectContext

typealias TransactionResult = Result<Void, CoreDataTransactionError>
typealias TransactionClosure = (TransactionResult) -> Void

enum CoreDataTransactionError: Error {
  case executing(Error)
  case transaction(Error)
  case parent(Error)

  func unwrap() -> Error {
    switch self {
      case let .executing(error): return error
      case let .transaction(error): return error
      case let .parent(error): return error
    }
  }
}

class CoreDataUnitOfWork {

  let parentContext: NSManagedObjectContext
  let currentContext: NSManagedObjectContext

  init(parentContext: NSManagedObjectContext, currentContext: NSManagedObjectContext) {
    self.parentContext = parentContext
    self.currentContext = currentContext
  }

  func execute(_ action: @escaping AtomicOperation, after closure: @escaping TransactionClosure) {
    self.currentContext.perform {
      _ = self.perform(action)
        .andThen(self.commitCurrentContext)
        .andThen(self.attempt({ self.commitParentContext(closure) }))
        .orElse(self.fail(closure))
    }
  }

  private func perform(_ action: @escaping AtomicOperation) -> TransactionResult {
    var result: TransactionResult = .success()
    do {
      try action(self.currentContext)
    } catch let error {
      let executingError = CoreDataTransactionError.executing(error)
      result = .error(executingError)
    }

    return result
  }

  private func commitCurrentContext() -> TransactionResult {
    var result: TransactionResult = .success()
    do {
      try self.currentContext.save()
    } catch let error {
      self.currentContext.rollback()
      let executingError = CoreDataTransactionError.transaction(error)
      result = TransactionResult.error(executingError)
    }

    return result
  }

  private func attempt(_ closure: @escaping () -> Void) -> () -> TransactionResult {
    return {
      closure()

      return .success()
    }
  }

  private func commitParentContext(_ closure: @escaping TransactionClosure) {
    self.parentContext.perform {
      do {
        try self.parentContext.save()
        closure(TransactionResult.success())
      } catch let error {
        self.parentContext.rollback()
        let executingError = CoreDataTransactionError.parent(error)
        closure(TransactionResult.error(executingError))
      }
    }
  }

  private func fail(_ closure: @escaping TransactionClosure) -> (CoreDataTransactionError) -> TransactionResult {
    return {
      error in
      closure(.error(error))

      return .success()
    }
  }

}
