//
//  Registry.swift
//  Repos
//
//  Created by funkill on 12/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//

import Foundation

// Fucking god object!
class Registry {

  internal static var logger: Logger = {
    #if DEBUG
      let handler = ConsoleHandler(level: .debug)
    #else
      let handler = NullHandler()
    #endif
    let logger = Journalist(named: Bundle.main.bundleIdentifier!, handled: [handler])

    return logger
  }()

  internal static var dbManager: DBManager = {
    let connection = CoreDataConnector()

    return DBManager(connection)
  }()

  internal static var restClient: RestClient = {
    #if (DEBUG && !TRACE_REST) || NO_NET
      let configuration = RestClientConfiguration.fileConfiguration
    #else
      let configuration = RestClientConfiguration.defaultConfiguration
    #endif

    #if TRACE_REST
      let logger = Registry.logger
    #else
      let logger = nil
    #endif
    let client = configuration.client.init(baseUrl: configuration.baseUrl, logger: logger)

    return RestClient(client)
  }()

}
