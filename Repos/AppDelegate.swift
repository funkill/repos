//
//  AppDelegate.swift
//  Repos
//
//  Created by funkill on 25/11/16.
//
//

import UIKit

import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  // swiftlint:disable:next line_length
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    #if arch(arm)
      Registry.logger.debug("Real device")
      Fabric.with([Crashlytics.self])
    #endif

    return true
  }

  func applicationWillTerminate(_ application: UIApplication) {
    Registry.dbManager.synchronize()
  }

}
