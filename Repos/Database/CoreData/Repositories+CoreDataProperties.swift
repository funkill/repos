//
//  Repositories+CoreDataProperties.swift
//  Repos
//
//  Created by funkill on 06/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData

extension Repositories {

  static let entityName = "Repositories"

  @nonobjc convenience init(context: NSManagedObjectContext) {
    let description = NSEntityDescription.entity(forEntityName: Repositories.entityName, in: context)!
    self.init(entity: description, insertInto: context)
  }

  @nonobjc public class func fetchRequest() -> NSFetchRequest<Repositories> {
      return NSFetchRequest<Repositories>(entityName: Repositories.entityName)
  }

  @NSManaged public var fullName: String?
  @NSManaged public var repositoryDescription: String?
  // swiftlint:disable:next variable_name
  @NSManaged public var repository_user: Users

}
