//
//  Repositories+CoreDataClass.swift
//  Repos
//
//  Created by funkill on 06/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData

@objc(Repositories)
public class Repositories: NSManagedObject {
  static func find(context: NSManagedObjectContext, name: String? = nil) -> [Repositories] {
    let request: NSFetchRequest<Repositories> = Repositories.fetchRequest()
    if name != nil {
      let predicate = NSPredicate(format: "fullName = %@", name!)
      request.predicate = predicate
    }

    var result: [Repositories] = []
    context.performAndWait {
      do {
        // swiftlint:disable:next force_cast
        result = try context.fetch(request as! NSFetchRequest<NSFetchRequestResult>) as! [Repositories]
      } catch let error {
        Registry.logger.error("Error on fetch request", context: ["Error": error])
      }
    }

    return result
  }

  static func exists(context: NSManagedObjectContext, name: String) -> Bool {
    let request: NSFetchRequest<Repositories> = Repositories.fetchRequest()
    request.predicate = NSPredicate(format: "fullName = %@", name)
    request.fetchLimit = 1
    var count = 0
    context.performAndWait {
      do {
      count = try context.count(for: request)
      } catch let error {
        Registry.logger.error("Error on fetch request", context: ["Error": error])
      }
    }

    // swiftlint:disable:next empty_count
    return count > 0
  }

  func save() {
    self.managedObjectContext!.insert(self)
  }

  func remove() {
    self.managedObjectContext!.delete(self)
  }
}
