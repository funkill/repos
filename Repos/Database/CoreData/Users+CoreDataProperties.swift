//
//  Users+CoreDataProperties.swift
//  Repos
//
//  Created by funkill on 06/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData

extension Users {

  static let entityName = "Users"

  @nonobjc convenience init(context: NSManagedObjectContext) {
    let description = NSEntityDescription.entity(forEntityName: Users.entityName, in: context)
    self.init(entity: description!, insertInto: context)
  }

  @nonobjc public class func fetchRequest() -> NSFetchRequest<Users> {
    return NSFetchRequest<Users>(entityName: Users.entityName)
  }

  @NSManaged public var email: String?
  @NSManaged public var name: String?
  // swiftlint:disable:next variable_name
  @NSManaged public var user_repositories: NSSet?

}

// MARK: Generated accessors for user_repositories
extension Users {

//    @objc(addUser_repositoriesObject:)
//    @NSManaged public func addToUser_repositories(_ value: Repositories)
//
//    @objc(removeUser_repositoriesObject:)
//    @NSManaged public func removeFromUser_repositories(_ value: Repositories)

    @objc(addUser_repositories:)
    @NSManaged public func addToUser_repositories(_ values: NSSet)

    @objc(removeUser_repositories:)
    @NSManaged public func removeFromUser_repositories(_ values: NSSet)

}
