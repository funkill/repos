//
//  Users+CoreDataClass.swift
//  Repos
//
//  Created by funkill on 06/01/17.
//  Copyright © 2017 E-legion. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData

@objc(Users)
public class Users: NSManagedObject {
  class func find(context: NSManagedObjectContext, name: String, email: String) throws -> [Users] {
    let predicate = NSPredicate(format: "name = %@ AND email = %@", name, email)
    let request: NSFetchRequest<Users> = Users.fetchRequest()
    request.predicate = predicate

    var result: [Users] = []
    context.performAndWait {
      do {
        // swiftlint:disable:next force_cast
        result = try context.fetch(request as! NSFetchRequest<NSFetchRequestResult>) as! [Users]
      } catch let error {
        Registry.logger.error("Error on fetch request", context: ["Error": error])
      }
    }

    return result
  }

  func save() {
    self.managedObjectContext!.insert(self)
  }
}
